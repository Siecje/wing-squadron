package squad;

import javax.swing.JFrame;

public class GameFrame extends JFrame{
    //want to pass either a tutorial object or a Window Object
    //but they extend JPanel
    //need them also to implement a Game class
    //polymorphism
    public GameFrame(int players){
        if(players == 1){
            //creates a Window object which extends JPanel
            Tutorial newTutorial = new Tutorial(this);
            //add the JPanel to the JFrame
            add(newTutorial);        
            //Listen to the JPanel
            addKeyListener(newTutorial);
            //super.dispose();    
            setTitle("Training");
        }
        else if (players == 2){
            //creates a Window object which extends JPanel
            TwoPlayerGame newGame = new TwoPlayerGame(this);
            //add the JPanel to the JFrame
            add(newGame);
            //Listen to the JPanel
            addKeyListener(newGame);
            setTitle("Shooting Game");
        }
                
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); to call newMenu?
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //width and height?
        setSize(800, 600);
        //place window in the middle of the screen
        setLocationRelativeTo(null);
        //setTitle(newGame.getTitle());
        setResizable(true);
        setVisible(true);
    }
}