package squad;

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JFrame;

//both tutorial and TwoPlayerGame have similiar methods and variables ????

public class TwoPlayerGame extends Game{
    //game plays for 3 minutes
    int gameTime = 180000;
    
    JFrame frame;
    
    //initilizes both players
        public TwoPlayerGame (JFrame frame){
            players.add(new Player(20, 20, player1Image, p1Up, p1Down, p1Left, p1Right, p1Shoot, 1000));
            players.add (new Player(20, 100, player2Image, p2Up, p2Down, p2Left, p2Right, p2Shoot, 1000));
            
            this.frame = frame;
        }
        
    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        super.paint(g2d);
        
        g2d.drawString("Time Remaining: "+ (gameTime -(time-startTime))/1000+"s", 20, 40);
        
        //housekeeping
        g.dispose();
        g2d.dispose();
        
        //Game Ends after 3 minutes
        if(System.currentTimeMillis()-startTime >= gameTime ){
           //DISPLAY THE WINNER
            //MENU
            frame.dispose();
            //System.exit(0);
            //new Menu();
        }
        //(time-startTime)/1000 > 3
        //enemiesSurvived > 100
        
        //loop
        repaint();
    }
}
        
//spawn offscreen
//not shooting each other
//players pass through each other zzzz
//make moer enemeis spawn as time goes on
//when an enemy passes by the spawn point