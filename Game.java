package squad;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

//both tutorial and TwoPlayerGame have similiar methods and variables ????

public class Game extends JPanel implements KeyListener{
    Sound shot = new Sound("shot.wav");
    boolean poweredUp;
    
    //load images once and re use them
    Image player1Image =  new ImageIcon(this.getClass().getResource("\\images\\player1.png")).getImage();
    Image player2Image =  new ImageIcon(this.getClass().getResource("\\images\\player2.png")).getImage();
    Image [] bulletImages =  {
        new ImageIcon(this.getClass().getResource("\\images\\bullet1.png")).getImage(), 
        new ImageIcon(this.getClass().getResource("\\images\\bullet2.png")).getImage()
    };
    Image bullet2Image =  new ImageIcon(this.getClass().getResource("\\images\\bullet2.png")).getImage();
    Image enemyImage =  new ImageIcon(this.getClass().getResource("\\images\\enemy1.png")).getImage();
    Image [] powerUpImages =  {
        new ImageIcon(this.getClass().getResource("\\images\\powerup1.png")).getImage(),
        new ImageIcon(this.getClass().getResource("\\images\\powerup2.png")).getImage(),
        new ImageIcon(this.getClass().getResource("\\images\\powerup3.png")).getImage()
    };

    
    int enemySize = enemyImage.getHeight(this);
    
    int enemiesSurvived = 0;
    
    long time;
    long startTime;
    
    //get image size for variable
    
    //??
    Random generator = new Random(19580427);
    int spawnHeight; //used to hold random spawn height 
    
    //Window Size
    int width = 800;
    int height = 600;
        
    //MOVE THIS??
    int p1Up = KeyEvent.VK_W;
    int p1Down = KeyEvent.VK_S;
    int p1Left = KeyEvent.VK_A;
    int p1Right = KeyEvent.VK_D;
    
    int p1Shoot = KeyEvent.VK_SPACE;
    
    int p2Up = KeyEvent.VK_UP;
    int p2Down = KeyEvent.VK_DOWN;
    int p2Left = KeyEvent.VK_LEFT;
    int p2Right = KeyEvent.VK_RIGHT;

    int p2Shoot = KeyEvent.VK_NUMPAD0;
    
    //Two player game
    ArrayList<Player> players = new ArrayList<Player>();    
    
        
    //powerUp delay
    private long powerUpLastGotTime;
    private long powerUpSpawnDelay = 6000;
    private int powerUpNum;
    
    
    //enemyspawn rate
    long enemyLastSpawnTime;
    long enemySpawnDelay = 800;
    
    
    //holds list of on screen enemies
    ArrayList<MovingObject> enemies = new ArrayList<MovingObject>();
    //holds list of on screen bullets
    ArrayList<MovingObject> bullets = new ArrayList<MovingObject>();
    //holds list of power ups
    ArrayList<MovingObject> powerUps = new ArrayList<MovingObject>();
   
    
    public Game() {
        startTime = System.currentTimeMillis();
        //double Buffering
        setDoubleBuffered(true);
        setFocusTraversalKeysEnabled(false);
        poweredUp = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        e.consume();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        //set constants to hold control configuration
        for(int i = 0;i<players.size();i++){
            players.get(i).updateDirection(key, true);
        }
        e.consume();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        //set constants to hold control configuration
        //used to stop moving in when a key is released
        //player1
        for(int i = 0;i<players.size();i++){
            players.get(i).updateDirection(key, false);
        }
        e.consume();
    }

    public void updateBullets(){
        //if shoot cooldown is finished
        
        //if player is shooting
        for(int i = 0;i<players.size();i++){
            if (players.get(i).isShooting() && System.currentTimeMillis() - players.get(i).getLastShot() >= players.get(i).getShotDelay()){
                //position does not change for number of bullets
                //change to j=0 to allow bllets to pass through 1 enemy
                for(int j=1;j<players.get(i).getNumShots();j++){
                    bullets.add(new MovingObject(players.get(i).getX()+players.get(i).getSize(), players.get(i).getY()+players.get(i).getSize()/2-bulletImages[i].getHeight(this)/2+bulletImages[i].getHeight(this), 0.5f, 0, bulletImages[i]));
                    bullets.add(new MovingObject(players.get(i).getX()+players.get(i).getSize(), players.get(i).getY()+players.get(i).getSize()/2-bulletImages[i].getHeight(this)/2-bulletImages[i].getHeight(this), 0.5f, 0, bulletImages[i]));
                }
                shot.playOnce();
                players.get(i).setShotTime(System.currentTimeMillis());
            }
        }
        
        //update all of the bullets position 
        for(int i = 0; i <bullets.size();i++){
            if (bullets.get(i).getX() < width){
                bullets.get(i).update();
            }
            //remove the bullet from the array if it is off the screen
            else {
                bullets.remove(i);
            }
        }
        
        for(int i = 0;i<players.size();i++){
            for(int j = 0;j<bullets.size();j++){
                if (players.get(i).collides(bullets.get(j))){
                    //determines which player shot the bullet and awards points
                    for(int k = 0;k<bulletImages.length;k++){
                        if(bullets.get(j).getImage() == bulletImages[k]){
                            players.get(k).addScore(1);
                            bullets.remove(j);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    public void updateEnemies(){
        //if a new enemy should spawn
        //if respawn timer cooldown has finished
        if (System.currentTimeMillis() - enemyLastSpawnTime >= enemySpawnDelay){
             //random y coord on screen NOT WORKING
            spawnHeight = generator.nextInt(height-enemySize-10);
            if(spawnHeight <= enemySize){
                spawnHeight+= enemySize;
            }
            
            enemies.add(new MovingObject(width+5, spawnHeight, -0.2f, 0, enemyImage));
            enemyLastSpawnTime = System.currentTimeMillis();
        }
        
        //move the enemies
        for(int i = 0;i<enemies.size();i++){
            enemies.get(i).update();
            if (enemies.get(i).getX()< 0 ){
                enemiesSurvived++;
                enemies.remove(i);
            }
        }
     
        //colision detectsion check if an enemy should be removed
        for(int h = 0;h<bullets.size();h++){
            for(int i = 0;i<enemies.size();i++){
            
                if (bullets.get(h).collides(enemies.get(i))){                    
                    for(int k = 0;k<bulletImages.length;k++){
                        if(bullets.get(h).getImage() == bulletImages[k]){
                            players.get(k).addScore(100);
                            bullets.remove(h);
                            enemies.remove(i);
                            break;
                        }
                    }
                    
                    
                    //remove the break to allow bullets to travel through enemies
                    break;
                }                
            }
        }
        
        //check if a player has run into an enemy
        for(int h = 0;h<enemies.size();h++){
            for(int i = 0;i<players.size();i++){
                if(enemies.get(h).collides(players.get(i))){
                        players.get(i).died();
                 }
            }
        }
        
        
    }
    
    public void updatePowerUps(){
        if(System.currentTimeMillis() - powerUpLastGotTime >= powerUpSpawnDelay){
            //remove current power up effects
            if(poweredUp){
                //restore enemies movement
                if (powerUpNum == 0){
                    for(int j= 0;j<enemies.size();j++){
                        //comment this out to freeze forever
                        enemies.get(j).setXVel(-0.2f);
                    }
                    enemySpawnDelay = 5000;
                    poweredUp = false;
                }
                //restore players velocity
                else if (powerUpNum == 1){
                    //just restore both players
                    for(int i = 0;i<players.size();i++){
                        players.get(i).setXVel(0.45f);
                        players.get(i).setYVel(0.45f);
                    }
                }
                //restore bullets?
                else if (powerUpNum == 2){
                    for(int i = 0;i<players.size();i++){
                        players.get(i).setShotDelay(1000);
                    }
                    
                }
            }
            
            spawnHeight = generator.nextInt(height-enemySize-10);
            if(spawnHeight <= 0){
                spawnHeight+= enemySize;
            }
            
            powerUpNum = generator.nextInt(powerUpImages.length);
            
            powerUps.add(new MovingObject(width+5, spawnHeight, -0.4f, 0, powerUpImages[powerUpNum]));
            
            powerUpLastGotTime = System.currentTimeMillis();
            
        }

        //update the power up position 
        for(int i = 0; i <powerUps.size();i++){
            if (powerUps.get(i).getX() > 0){
                powerUps.get(i).update();
            }
            //remove the power up from the array if it is off the screen
            else {
                powerUps.remove(i);
            }
        }

        //colision detectsion check if a player is touching a power up
        for(int h = 0;h<powerUps.size();h++){
            for(int i = 0;i<players.size();i++){

                if(powerUps.get(h).collides(players.get(i))){
                    players.get(i).addScore(100);
                    powerUps.remove(h);
                    poweredUp = true;
                    if (powerUpNum == 0){
                        //stop enemies
                        for(int j= 0;j<enemies.size();j++){
                            enemies.get(j).xVel = 0;
                            enemies.get(j).yVel = 0;
                        }
                        //increase spawn delay
                        enemySpawnDelay+=1000;
                        powerUpLastGotTime = System.currentTimeMillis();
                        break;
                    }
                    //make player move faster
                    else if(powerUpNum ==1){
                        players.get(i).setXVel(players.get(i).getXVel()+.2f);
                        players.get(i).setYVel(players.get(i).getYVel()+.2f);
                        break;
                    }
                    else if(powerUpNum ==2){
                        players.get(i).setShotDelay(players.get(i).getShotDelay()-750);
                        break;
                    }                    
                }
            }
        }
        
    }
    
    public void updatePlayers(){
        for(int i = 0;i<players.size();i++){
            float x,y;
            
            x = players.get(i).getXPos();
            y = players.get(i).getYPos();
            
            if (players.get(i).isMovingUp() && players.get(i).getYPos() > 0 ){
                players.get(i).setYPos(players.get(i).getYPos() - players.get(i).getYVel());
            }
            if (players.get(i).isMovingDown() && players.get(i).getYPos()+players.get(i).getImage().getHeight(null) < 600-players.get(i).getImage().getHeight(null)){
                players.get(i).setYPos(players.get(i).getYPos() + players.get(i).getYVel());
            }
            if (players.get(i).isMovingLeft() && players.get(i).getXPos() > 0){
                players.get(i).setXPos(players.get(i).getXPos() - players.get(i).getXVel());                
            }
            if (players.get(i).isMovingRight() && players.get(i).getXPos()+ players.get(i).getImage().getWidth(null) < 800-players.get(i).getImage().getWidth(null)){
                players.get(i).setXPos(players.get(i).getXPos() + players.get(i).getXVel());
            }

            for(int j =0;j<players.size();j++){
                if(i==j)
                    continue;
                if(players.get(i).collides(players.get(j))){
                    players.get(i).setXPos(x);
                    players.get(i).setYPos(y);
                    break;
                }
                    
            }

        }
    }
    
    @Override
    public void paint(Graphics g) {
        time = System.currentTimeMillis();
        //update player positions
        
        //update players bullets
        updateBullets();
        //update enemy locations
        updateEnemies();
        //update power up locations
        updatePowerUps();
        
        updatePlayers();
        
        //clear the screen
        g.clearRect(0, 0, width, height);
        
        //why isn't the background black
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 800, 600);
        
        
        //why do I even need this
        Graphics2D g2d = (Graphics2D)g;
        //g2d.setColor(Color.BLACK);
        //g2d.drawRect(0, 0, 300, 280);
        
        //draw the players
        for(int i = 0;i<players.size();i++){
            g2d.drawImage(players.get(i).getImage(), (int)players.get(i).getX(), (int)players.get(i).getY(), this);
        }
        
        //drawBullets();
        for(int i = 0;i<bullets.size();i++){
            g2d.drawImage(bullets.get(i).getImage(), (int)bullets.get(i).getX(), (int)bullets.get(i).getY(), this);
        }
        
        //drawEnemies
        for(int i = 0;i<enemies.size();i++){
            g2d.drawImage(enemies.get(i).getImage(), (int)enemies.get(i).getX(), (int)enemies.get(i).getY(), this);
        }
        
        //drawPowerUps
        for(int i = 0;i<powerUps.size();i++){
            g2d.drawImage(powerUps.get(i).getImage(), (int)powerUps.get(i).getX(), (int)powerUps.get(i).getY(), this);
        }
        g2d.setFont(new Font("sansserif", Font.BOLD, 22));
        g.setColor(Color.WHITE);
        
        
        g2d.drawString("Enemies Missed: "+ enemiesSurvived, 20, 20);
        for(int i = 0;i<players.size();i++){
            g2d.drawString("Player "+ (i+1) +" Score: "+ players.get(i).getScore(), 20, 65+25*i);
        }
        
        //housekeeping
        //g.dispose();
        //g2d.dispose();
        
    }
}        
//make moer enemeis spawn as time goes on
//bullets travel through enemies once