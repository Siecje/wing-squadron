package squad;

import java.awt.Image;

//-10 lives is infinite lives
//both player and bullet have similiar methods and variables ????

public class Player extends MovingObject{
    private int deaths;
    //used to move on angles and smoothly
    private boolean movingUp;
    private boolean movingDown;
    private boolean movingLeft;
    private boolean movingRight;
    
    //controls
    private int upButton;
    private int downButton;
    private int leftButton;
    private int rightButton;
    
    private int shootButton;
    
    //if currently shooting
    private boolean shooting;
    
    //how often you can shoot in miliseconds
    private long shotDelay;
    private long lastShotTime;
    
    private int numberOfShots;
    
    private int score;
    
    public Player(float initialX, float initialY, Image loadedImage, int upButton, int downButton, int leftButton, int rightButton, int shootButton, int shotDelay){
        super(initialX, initialY, 0.45f, 0.45f, loadedImage);

        this.upButton = upButton;
        this.downButton = downButton;
        this.leftButton = leftButton;
        this.rightButton = rightButton;
        
        this.shootButton = shootButton;
        
        this.shotDelay = shotDelay;
        numberOfShots = 2;
        score = 0;
    }
    
    public void updateDirection(int key, boolean state){
        if (key == upButton){
            setMovingUp(state);
        }
        else if (key == downButton){
            setMovingDown(state);
        }
        else if (key == leftButton){
            setMovingLeft(state);
        }
        else if (key == rightButton){
            setMovingRight(state);
        }
        else if (key == shootButton){
            setShooting(state);
        }
    }  
    
    /*
    public void updateLocation (){
                if (movingUp && yPos > 0 ){
                yPos -= yVel;
            }
            if (movingDown && yPos+image.getHeight(null) < 600-image.getHeight(null)){
                yPos += yVel;
            }
            if (movingLeft && xPos > 0){
                xPos -= xVel;
            }
            if (movingRight && xPos+image.getWidth(null) < 800-image.getWidth(null)){
                xPos += xVel;
            }
        
    }
    */
    
    public void died(){
        deaths++;
        
        xPos = 0;
        yPos = 0;
        
        score -=100;
        //restore weapons?
    }
    
    public int getDeaths(){
        return deaths;
    }
    
    public boolean isMovingUp(){
        return movingUp;
    }
    
    public void setMovingUp(boolean moving){
        movingUp = moving;
    }
    
    public boolean isMovingDown(){
        return movingDown;
    }
    
    public void setMovingDown(boolean moving){
        movingDown = moving;
    }
    
    public boolean isMovingLeft(){
        return movingLeft;
    }
    
    public void setMovingLeft(boolean moving){
        movingLeft = moving;
    }
    
    public boolean isMovingRight(){
        return movingRight;
    }
    
    public void setMovingRight(boolean moving){
        movingRight = moving;
    }
    
    public boolean isShooting(){
        return shooting;
    }
    
    public void setShooting(boolean shooting){
        this.shooting  = shooting;
    }

    public void setShotTime(long time){
        lastShotTime = time;
    }
    
    public long getLastShot(){
        return lastShotTime;
    }
    
    public long getShotDelay(){
        return shotDelay;
    }
    
    public void setShotDelay(long delay){
        shotDelay = delay;
    }
    
    public int getNumShots(){
        return numberOfShots;
    }
    
    public void addScore(int addition){
        score += addition;
    }
    
    public int getScore(){
        return score;
    }
    
    public float getYPos(){
        return yPos;
    }
    
    public float getXPos(){
        return xPos;
    }
    
    public void setXPos(float xPos){
        this.xPos = xPos;
    }
    
    public void setYPos(float yPos){
        this.yPos = yPos;
    }
    
}