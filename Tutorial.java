package squad;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;

//both tutorial and TwoPlayerGame have similiar methods and variables ????
//windowSize??

public class Tutorial extends Game{
    String [] hints = {"Use W-A-S-D to move", "Press Space to Shoot", "Collect Power Ups to stop enemies"};
    int hintNum = -1;
    
    long lastHintTime;
    long hintDelay = 10000;
    
    JFrame frame;
    
    public Tutorial (JFrame frame){
        //initilizes both players
        players.add(new Player( 20, 20, player1Image, p1Up, p1Down, p1Left, p1Right, p1Shoot, 1000));
        this.frame = frame;
    }    
    
    @Override
    public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_ESCAPE){
            frame.dispose();
        }
        e.consume();
    }
    
    @Override
    public void paint(Graphics g) {
        
        
        
        Graphics2D g2d = (Graphics2D)g;
        super.paint(g2d);
        
        g2d.drawString("Press Esc to return to main menu ", 250, 20);
        
        if(System.currentTimeMillis()-lastHintTime >= hintDelay){
            hintNum++;
            lastHintTime = System.currentTimeMillis();
        }
        if (hintNum >= hints.length){
            hintNum = 0;
        }
        
        g2d.drawString(hints[hintNum], 20, 200);
        
        //housekeeping
        g.dispose();
        g2d.dispose();
        //
        //checks if all the lives are gone
        //if(players.get(0).getLives()<=0 && players.get(1).getLives() <= 0){
          // System.exit(0);
            //new Menu();
        //}
        //(time-startTime)/1000 > 3
        //enemiesSurvived > 100
        
        
        //loop
        repaint();
    }
}