/*
 * Used for enemies and bullets
 */

//both player and bullet have similiar methods and variables ????

package squad;

import java.awt.Image;

public class MovingObject {
    //location
    protected float xPos;
    protected float yPos;
    
    //speed in the given direction
    protected float xVel;
    protected float yVel;

    protected Image image;
    
    protected int size;

    public MovingObject(float x, float y, float velX, float velY, Image loadedImage){
        xPos = x;
        yPos = y;
        xVel = velX;
        yVel = velY;
        image = loadedImage;
        size = image.getWidth(null);
    }

    public void update(){
        xPos += xVel;
        yPos += yVel;
    }
    
    public float getX(){
        return xPos;
    }
    
    public void setX(float x){
        xPos = x;
    }
    
    public float getY(){
        return yPos;
    }
    
    public void setY(float y){
        yPos = y;
    }
    
    public float getXVel(){
        return xVel;
    }
    
    public void setXVel(float xVel){
        this.xVel = xVel;
    }
    
    public float getYVel(){
        return yVel;
    }
    
    public void setYVel(float yVel){
        this.yVel = yVel;
    }
    
    public Image getImage(){
        return image;
    }
    
    public void setImage(Image image){
        this.image = image;
    }
    
    public int getSize(){
        return size;
    }
    
     public boolean collides(MovingObject check){
        
         if (((this.getX() >= check.getX() && this.getX() <= check.getX()+ check.getSize())
                        || (check.getX() >= this.getX() && check.getX() <= this.getX()+this.getSize()))
                        && ((this.getY() >= check.getY() && this.getY() <= check.getY()+check.getSize())
                        || (check.getY() >= this.getY() && check.getY() <= this.getY()+this.getSize()))
                        
                ){
            return true;
        }
        return false;
        
        /* 
        Rectangle rect1 = new Rectangle(this.getSize(), this.getSize());
        Rectangle rect2 = new Rectangle (check.getSize(), check.getSize());
        return rect1.intersects(rect2);
        */
    }
}
