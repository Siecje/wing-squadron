package squad;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class Menu extends JFrame{
    JFrame menu;
    
    public class TutorialButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            //make a game object here and pass it to GameFrame?
            //menu.dispose();
            //menu.setVisible(false);
            GameFrame newGame = new GameFrame(1);
            //new GameFrame(1)??
        }
    }
    
    public class GameButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            //make a game object here and pass it to GameFrame?
            //menu.setVisible(false);
            GameFrame newGame = new GameFrame(2);
        }
    }
    
    public Menu(){
        
        
        menu = new JFrame();
        
        menu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //width and height?
        menu.setSize(800, 600);
        //place window in the middle of the screen
        menu.setLocationRelativeTo(null);
        menu.setTitle("Shooting Menu");
        menu.setResizable(true);
        menu.setVisible(true);

        JButton playButton = new JButton ("2 Player Game");
        JButton tutorialButton = new  JButton("1 Player Training");
        
        GameButtonListener whenClicked = new GameButtonListener();
        TutorialButtonListener clicked = new TutorialButtonListener();

        playButton.addActionListener(whenClicked);
        tutorialButton.addActionListener(clicked);

        menu.setLayout(new GridLayout());
        
        menu.add(playButton);
        menu.add(tutorialButton);
    }
    
    public static void main(String[] args) {
        Sound music = new Sound("music.wav");
        music.repeatPlay();
        
        //Initially loads the menu
        
        Menu menu = new Menu();
    }
}