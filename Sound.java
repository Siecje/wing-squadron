package squad;


import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sound{
    //private final static String SOUND_DIR = "Sounds\\";
        
    private File songPath; // Sound path
    private AudioInputStream audioIn;
    
    private Clip clip;    

    public Sound(String fileName){
        try{
            // Open an audio input stream.
            songPath = new File(fileName);
            audioIn = AudioSystem.getAudioInputStream(songPath);
            // Get a sound clip resource.
            clip = AudioSystem.getClip();
            clip.open(audioIn);

        } catch(Exception e){
            System.out.println(e.getMessage());
        } // Satisfy the catch       
    }
    
    public void playOnce(){
        clip.setFramePosition(0);
        clip.start();
    }
    
    public void repeatPlay(){
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }
}